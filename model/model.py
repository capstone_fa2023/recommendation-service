import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from recommendation.data_util import add_dense_feature


class RecModel(nn.Module):
    def __init__(
        self,
        hidden_size,
        n_items,
        embedding_dim,
        num_features,
        n_layers=1,
        dropout=0.25,
    ):
        super(RecModel, self).__init__()
        self.hidden_size = hidden_size
        self.n_items = n_items
        self.embedding_dim = embedding_dim
        self.n_layers = n_layers
        self.embedding = nn.Embedding(self.n_items, self.embedding_dim, padding_idx=0)
        self.num_features = num_features
        self.emb_dropout = nn.Dropout(dropout)
        self.gru = nn.GRU(
            self.embedding_dim + self.num_features, self.hidden_size, self.n_layers
        )
        self.a_1 = nn.Linear(self.hidden_size, self.hidden_size, bias=False)
        self.a_2 = nn.Linear(self.hidden_size, self.hidden_size, bias=False)
        self.v_t = nn.Linear(self.hidden_size, 1, bias=False)
        self.ct_dropout = nn.Dropout(0.5)
        self.b = nn.Linear(
            self.embedding_dim + self.num_features, 2 * self.hidden_size, bias=False
        )
        self.sf = nn.Softmax()
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def forward(self, input_seq, input_lengths, hidden=None):
        embedded = self.embedding(input_seq)
        embedded, new_hidden_size = add_dense_feature(
            input_seq, embedded, self.num_features
        )
        packed = pack_padded_sequence(embedded, input_lengths)
        outputs, hidden = self.gru(packed, hidden)
        outputs, length = pad_packed_sequence(outputs)
        ht = hidden[-1]  # (batch_size, hidden_size)
        outputs = outputs.permute(1, 0, 2)  # [batch_size, max_length, hidden_size]
        c_global = ht
        gru_output_flatten = outputs.contiguous().view(-1, self.hidden_size)
        q1 = self.a_1(gru_output_flatten).view(outputs.size())
        q2 = self.a_2(ht)  # (batch_size, hidden_size)
        mask = torch.where(
            input_seq.permute(1, 0) > 0,
            torch.tensor([1.0], device=self.device),
            torch.tensor([0.0], device=self.device),
        )  # batch_size x max_len
        q2_expand = q2.unsqueeze(1).expand_as(
            q1
        )  # shape [batch_size, max_len, hidden_size]
        q2_masked = (
            mask.unsqueeze(2).expand_as(q1) * q2_expand
        )  # batch_size x max_len x hidden_size
        alpha = self.v_t(torch.sigmoid(q1 + q2_masked).view(-1, self.hidden_size)).view(
            mask.size()
        )  # batch_size x max_len
        alpha_exp = alpha.unsqueeze(2).expand_as(
            outputs
        )  # batch_size x max_len x hidden_size
        c_local = torch.sum(alpha_exp * outputs, 1)  # (batch_size x hidden_size)
        c_t = torch.cat([c_local, c_global], 1)  # batch_size x (2*hidden_size)
        c_t = self.ct_dropout(c_t)

        item_indices = torch.arange(self.n_items).to(self.device)  # n_items
        item_indices = item_indices.unsqueeze(1)  # n_items x 1
        item_embs = self.embedding(item_indices)  # n_items x 1 x embedding_dim
        item_embs, n = add_dense_feature(
            item_indices, item_embs, self.num_features
        )  # n_items x 1 x (embedding_dim+num_features)
        item_embs = item_embs.squeeze(1)  # n_items x (embedding_dim+num_features)
        B = self.b(item_embs).permute(1, 0)  # (2*hidden_size) x n_items
        scores = torch.matmul(c_t, B)  # batch_size x n_items
        return scores
