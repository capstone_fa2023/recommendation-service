import torch
from torch.nn.utils.rnn import pad_sequence
from recommendation.util import get_tracks_feature_data, get_track_ids
import numpy as np
import os
import pickle
from app.function import (
    connect_mongo,
    insert_web_data,
    get_data_date,
    insert_listen_history,
    get_user_streaming_history,
)
import pandas as pd
from datetime import datetime

feature_data = get_tracks_feature_data()
db_user_history = "mev_user_history"
list_track_id = get_track_ids()


def load_data(
    root="data",
    valid_portion=0.1,
    maxlen=20,
    sort_by_len=False,
    train_set=None,
    test_set=None,
):
    # Load the dataset
    if train_set is None and test_set is None:
        path_train_data = os.path.join(root, "train.pkl")
        path_test_data = os.path.join(root, "test.pkl")
        with open(path_train_data, "rb") as f1:
            train_set = pickle.load(f1)

        with open(path_test_data, "rb") as f2:
            test_set = pickle.load(f2)

    if maxlen:
        new_train_set_x = []
        new_train_set_y = []
        for x, y in zip(train_set[0], train_set[1]):  # x = [214652220], y = 214840483
            if len(x) < maxlen:
                new_train_set_x.append(x)
                new_train_set_y.append(y)
            else:
                new_train_set_x.append(x[:maxlen])
                new_train_set_y.append(y)
        train_set = (new_train_set_x, new_train_set_y)
        del new_train_set_x, new_train_set_y

        new_test_set_x = []
        new_test_set_y = []
        for xx, yy in zip(test_set[0], test_set[1]):
            if len(xx) < maxlen:
                new_test_set_x.append(xx)
                new_test_set_y.append(yy)
            else:
                new_test_set_x.append(xx[:maxlen])
                new_test_set_y.append(yy)
        test_set = (new_test_set_x, new_test_set_y)
        del new_test_set_x, new_test_set_y

    train_set_x, train_set_y = train_set
    n_samples = len(train_set_x)
    sidx = np.arange(n_samples, dtype="int32")
    np.random.shuffle(sidx)
    n_train = int(np.round(n_samples * (1.0 - valid_portion)))
    valid_set_x = [train_set_x[s] for s in sidx[n_train:]]
    valid_set_y = [train_set_y[s] for s in sidx[n_train:]]
    train_set_x = [train_set_x[s] for s in sidx[:n_train]]
    train_set_y = [train_set_y[s] for s in sidx[:n_train]]

    (test_set_x, test_set_y) = test_set

    def len_argsort(seq):
        return sorted(range(len(seq)), key=lambda x: len(seq[x]))

    if sort_by_len:
        sorted_index = len_argsort(test_set_x)
        test_set_x = [test_set_x[i] for i in sorted_index]
        test_set_y = [test_set_y[i] for i in sorted_index]

        sorted_index = len_argsort(valid_set_x)
        valid_set_x = [valid_set_x[i] for i in sorted_index]
        valid_set_y = [valid_set_y[i] for i in sorted_index]

    train = (train_set_x, train_set_y)
    valid = (valid_set_x, valid_set_y)
    test = (test_set_x, test_set_y)
    return train, valid, test


def collate_fn(data):
    data.sort(key=lambda x: len(x[0]), reverse=True)
    lens = [len(sess) for sess, label in data]
    labels = []
    inputs = []
    for s, l in data:
        inputs.append(torch.LongTensor(s))
        labels.append(l)
    padded_sesss = pad_sequence(inputs)
    return padded_sesss, torch.tensor(labels).long(), lens


def add_dense_feature(input_seq, embeddings, num_features):

    input_expand = input_seq.view(-1, 1).expand(-1, num_features).clone().float()
    for i, input_item in enumerate(input_seq.flatten()):
        if input_item.item() == 0:
            continue
        input_expand[i] = torch.tensor(
            feature_data.get(input_item.item(), [0] * num_features)
        )

    input_expand = input_expand.view(
        input_seq.shape[0], input_seq.shape[1], num_features
    )
    outputs = torch.cat((input_expand, embeddings), 2)
    new_hidden_size = embeddings.shape[2] + num_features

    return outputs, new_hidden_size


def _preprocess_sess_dict(sessDict):
    sessDict = list(sessDict.items())
    itemIds = [item[1] for item in sessDict]
    inp_seq = []
    labels = []

    for i in range(len(sessDict)):
        if i >= 1:
            inp_seq += [itemIds[:i]]
            labels += [itemIds[i]]

    return inp_seq, labels, itemIds


def _preprocess_data(data_sess):
    inp_seqs = []
    labels = []
    sequences = []
    sessIds = list(data_sess.index)
    for sessId in sessIds:
        sessDict = data_sess.loc[sessId, "Sequence"]
        inp_seq, label, sequence = _preprocess_sess_dict(sessDict)
        inp_seqs += inp_seq
        labels += label
        sequences += sequence
    return inp_seqs, labels, sequences


def get_data_continue_train(seq_len: int = 20):
    mongo_client = connect_mongo()
    mev_history = mongo_client.get_database(db_user_history)

    collections = mev_history.list_collection_names()

    data_total = []
    for coll in collections:
        print(f"user id: {coll}")
        collection = mev_history[coll]
        data_raw = list(
            collection.find(
                {"isTrain": False, "trackId": {"$in": list_track_id}},
                {"trackId": 1, "timestamp": 1, "_id": 1},
            )
        )
        if len(data_raw) == 0:
            print("No data")
            continue
        else:
            for d in data_raw:
                d.update({"timestamp": d["timestamp"].strftime(r"%Y/%m/%d-%H:%M:%S")})
                d.update({"trackId": list_track_id.index(d["trackId"])})
            data_total.extend(data_raw)

            # id_list  = [d['_id'] for d in data_raw]

            # collection.update_many(
            #     {
            #         "_id": {"$in": id_list}
            #     },
            #     {
            #          "$set": {"isTrain": True}
            #     }
            # )

    print("total data len: ", len(data_total))
    data = []
    for i in range(0, len(data_total), seq_len):
        data_seq = data_total[i : i + seq_len]
        data_seq = {d["timestamp"]: d["trackId"] for d in data_seq}
        data.append([data_seq])

    data_df = pd.DataFrame(data)
    data_df.columns = ["Sequence"]

    train_data = data_df.sample(frac=0.9)
    test_data = data_df.drop(train_data.index)
    train_inp_seqs, train_labs, train_sequences = _preprocess_data(train_data)
    test_inp_seqs, test_labs, test_sequences = _preprocess_data(test_data)
    train = (train_inp_seqs, train_labs)
    test = (test_inp_seqs, test_labs)

    return train, test


if __name__ == "__main__":
    get_data_continue_train()
