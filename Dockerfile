FROM python:3.8-slim

ENV MONGO_USER=user
ENV MONGO_PASSWORD=pass_word
ENV WORKDIR=/rec_sys
ENV PYTHONPATH=${WORKDIR}

WORKDIR ${WORKDIR}

COPY ./app ./app
COPY ./data ./data
COPY ./model ./model
COPY ./recommendation ./recommendation
COPY requirements.txt requirements.txt

RUN pip install -r ./requirements.txt --no-cache-dir
CMD ["python", "app/main.py"]

EXPOSE 8001
